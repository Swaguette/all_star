import funk
import numpy as np
import matplotlib.pyplot as plt
import math
from astropy.io import fits
import operator
import matplotlib

matplotlib.rcParams.update({'errorbar.capsize': 2})

def wrap_f(arr, f):
    vfunc = np.vectorize(f)
    return vfunc(arr)

'''
img = funk.Image(r"M3/M3-L.fit")
#print(img.get_circ_flux((563, 1300, 24)))

#img.slice(0, 500, 0, 500)

img.scale(lambda x: wrap_f(x, lambda y: math.log(1 + y)), .5, 99.5)
#img.scale(np., .5, 99.5)

img.find_stars()
img.calc_flux()

img.plot()

'''

#fits_list = funk.FitsList(r"M13/M13-L.fit", r"M13/M13-R.fit", r"M13/M13-G.fit", r"M13/M13-B.fit", #(100, 2000, 100, 2000),
fits_list = funk.FitsList(r"M3/M3-L.fit", r"M3/M3-R.fit", r"M3/M3-G.fit", r"M3/M3-B.fit", #(100, 2000, 100, 2000),
#(1000, 3500, 1000, 3500))
#                            scale_func=lambda x: x)
                           scale_func=lambda x: wrap_f(x, lambda y: math.log(1 + y)))
#for i in fits_list.data[0].raw_data[2343, 1300:1400]:
#    print(i)
#print(fits_list.data[0].get_circ_flux((684,2667,20)))

BminV = []
V = []
V_err = []
BminV_err = []

for i in range(0, len(fits_list.data[0].stars)):
    B_ = fits_list.data[3].stars[i]
    V_ = fits_list.data[2].stars[i]
    R_ = fits_list.data[1].stars[i]

   # fits_list.data[2].calc_star_radius(V_.x, V_.y)
    
    if not B_.flux or not V_.flux or not R_.flux or -2.5 * math.log10(V_.flux) + 9.64 > 4.5:  
        continue
    #print("R flux: {}, V flux: {}, V/R: {}, -2.5log V/R: {}".format(R_.flux, V_.flux, V_.flux/R_.flux, -2.5*math.log10(V_.flux/R_.flux)))
    print("(x, y, r): ({}, {}, {}), bf: {}, vf: {}, V: {}, B-V: {}".format(B_.x, B_.y, B_.r, B_.flux, V_.flux, -2.5*math.log10(V_.flux)+29.728-5*math.log10(10400), -2.5*math.log10(B_.flux/V_.flux)-.058), flush=True)
    
    B_mag = -2.5*math.log10(B_.flux)
    V_mag = -2.5*math.log10(V_.flux)
    B_corr = 23.6 - (1./10)*B_mag
    V_corr = 23.5 - (1./8)*V_mag
#    B_corr = 24.46
#    V_corr = 24.94
#    B_corr = -0.089 * B_mag + 23.08#0.91 * B_mag + 36.894
#    V_corr = -0.2048*V_mag + 22.99
#    B_corr = 24.83
#    V_corr = 24.9#5.23
    B_mag += B_corr
    V_mag += V_corr

    # B: 24.670, V: 24.728
    BminV.append(B_mag - V_mag)#2.5 * math.log10(B_.flux / V_.flux) - 0.058)


    # distance: M3 10400, M13 6800
    distance = 10400
    # CORRECTIE:
    # 24.728 for M3, 24.39 for M13
#    V.append(V_mag+5-5*math.log10(distance))#-2.5 * math.log10(V_.flux) + 24.728 + 5 - 5 * math.log10(distance))
    V.append(V_mag)

    berr = 2.5*math.log10(1+1/math.sqrt(B_.flux))
    verr = 2.5*math.log10(1+1/math.sqrt(V_.flux))
    BminV_err.append(math.sqrt(berr**2+verr**2))
    V_err.append(verr)

#print(BminV, V, flush=True)
#plt.plot(BminV, V, 'ro', markersize=.3)
#for i in range(0, len(V)):
#    plt.errorbar(BminV[i], V[i], 'ro', xerr=BminV_err[i], yerr=V_err[i])

#plt.show()

'''
#rr = [s.r for s in fits_list.data[0].stars]
#plt.hist(fl)#, bins=100)
#plt.hist(fl, bins=[i / 20. for i in range(0, 20)])
#plt.show()



BminV = [-2.5 * math.log10(B.flux / V.flux) for B, V in zip(fits_list.data[3].stars, fits_list.data[2].stars) if V.flux]
V = [10 - 2.5 * math.log10(s.flux) for s in fits_list.data[2].stars if s.flux]

plt.plot(BminV, V, 'ro')
plt.show()

#fits_list.plot()

'''

# import each isochrone 
isochrones = []
data = []
with open('tmp1529665131.iso') as fp:
#with open('c') as fp:
    for row in fp:
        if row.split():
            if row[0] == '#':
                continue
            else:
                data.append(row.split())
        elif data:
            isochrones.append(data)
            data = []

#isochrones = [isochrones[26]]

# finds the tuple in L=[(), ...] closest to (bminv, v)
# returns (distance, index)
def hr_closest(L, bminv, v):
    dists = map(lambda x: math.sqrt((bminv - x[0])**2 + (v - x[1])**2), L)
#    dists = map(lambda x: bminv - x          [0], L)
    return min(enumerate(dists), key=operator.itemgetter(1))
#    return min(dists)


# try to fit each isochrone
stddevs = []
for i, L in enumerate(isochrones):
    #print("new iso fit")
    # the points on the isochrone
    isoB = [float(a[6]) for a in L]
    isoV = [float(a[7]) for a in L]
    isoBminV = [b - v for b, v in zip(isoB, isoV)]
    iso_data = list(zip(isoBminV, isoV))

#    dist_range = list(range(7500, 12500, 250))
    dist_range = [11000]
#    best_stddevs = []
    dist_stddevs = []
    for cluster_dist in dist_range:
        # the squared distances
        dist_sqr = []
        
        bverrs = []
    
        # for each point in HR diagram
        for k, ziip in enumerate(zip(BminV, V)):
            bminv, v = ziip
            j, closest = hr_closest(iso_data, bminv, v + 5 - 5*math.log10(cluster_dist))
            bverr = BminV_err[j]
    #        closest = hr_closest(iso_data, bminv, v)
    #        bverr = bverr**2
            bverrs.append(bverr)
            dist_sqr.append(closest**2 / bverr)
        stddev = sum(dist_sqr)/len(V)#math.sqrt(sum(dist_sqr) / len(V))
        print("i: {}, cluster_dist: {}, stddev: {}".format(i, cluster_dist, stddev), flush=True)
        dist_stddevs.append([i, cluster_dist, stddev])
    stddevs.append(min(dist_stddevs, key=operator.itemgetter(2)))

fig, ax1 = plt.subplots()
best_fit = min(stddevs, key=operator.itemgetter(2))
# plot actual data
offset = 5 - 5*math.log10(best_fit[1])
ax1.errorbar(BminV, V+np.ones_like(V)*offset, fmt='ro', markersize=0.5, yerr=V_err, xerr=BminV_err, elinewidth=.5, capsize=.2, label='Meetdata')
ax1.invert_yaxis()


# plot best fit in HR diagram
fit_BminV = [float(s[6]) - float(s[7]) for s in isochrones[best_fit[0]]]
fit_V = [float(s[7]) for s in isochrones[best_fit[0]]]
ax1.plot(fit_BminV, fit_V, color='blue', label='Isochroonfit')
ax1.set_xlim(-1, 3)
ax1.set_ylim(8, -8)
ax1.set_xlabel('B-V')
ax1.set_ylabel('V (absolute)')
ax1.legend()

ax2 = ax1.twinx()
ax2.set_ylabel('V (visual)')
ax2.set_ylim(8 - offset, -8 - offset)
#ax2.plot(BminV, V, 'ro', markersize=0.5)
#ax2.plot(fit_BminV, fit_V, color='blue')


print("Fitted to isochrone with age {} Gy, stddev={}, distance={}".format(best_fit[0]*(15-1.)/len(isochrones) + 1, best_fit[2], best_fit[1]))
fig.tight_layout()
plt.show()

