import astropy
from astropy.io import fits
import numpy as np
import pandas as pd
import skimage.exposure as skie
import skimage.feature as feat
import matplotlib.pyplot as plt
import copy
import operator

from astropy.stats import mad_std

import math

def gaussian_2D(height, x0, y0, r):
    return lambda x, y: height * math.exp(
            -.5*(((x - x0)/r)**2 + ((y-y0)/r)**2))

class Star:
    def __init__(self, x, y, r, flux=0):
        self.x = x
        self.y = y
        self.r = r
        self.flux = flux


class Image:
    def __init__(self, filename="", bg=0):
        if filename == "":
            return
        print("Init...", end='', flush=True)
        self.img = fits.getdata(filename)

        bg = np.median(self.img)
        print("MEEEEEDDIIIIAIAAANNNN: ", bg)
#        print(filename, ": ", avg)
#        vfunc = np.vectorize(lambda p: max(0, p - bg))
#        self.img = vfunc(self.img)
        self.img = self.img - bg * np.ones_like(self.img)
        self.img = np.clip(self.img, a_min=0, a_max=None)
        self.raw_data = np.copy(self.img)
        self.width, self.height = self.img.shape
        print("Done")

    def slice(self, x1, x2, y1, y2):
        self.img = self.img[x1:x2, y1:y2]
        self.raw_data = self.raw_data[x1:x2, y1:y2]
        self.width, self.height = self.img.shape
        return

    def normalize_raw(self):
        # TODO divide by maximum value
        self.raw_data = self.raw_data / self.raw_data.max()

    # transforms img with f(img)
    # and scales to percentile [l, h]
    def scale(self, f, l, h):
        
        # normalize
        #tmp_img = tmp_img / tmp_img.max()
        self.img = self.img / self.img.max()
        tmp_img = f(self.img)

        # take certain percentile, set as range
        low = np.percentile(tmp_img, l)
        high = np.percentile(tmp_img, h)
        self.img = skie.exposure.rescale_intensity(tmp_img, in_range=(low, high))

        # OS X compatibility
        self.img = self.img / self.img.max()


    def find_stars(self):
        print("Finding stars...", end='', flush=True)
        # store stars in self.stars, element format (y, x, r)
        #self.stars = feat.blob_log(self.img, max_sigma=8, num_sigma=50, threshold=.01)
#        self.stars.dump('m13_dump')
#        self.stars.dump('arr_dump')
#        self.stars = np.load('m13_dump')
        self.stars = np.load('arr_dump')
        self.stars = np.array([s for s in self.stars if 100<s[0]<4000 and 100<s[1]<4000])
        '''
        middle_slice = self.raw_data[2009:2135, 1523:1657]
        middle_slice = middle_slice / middle_slice.max()
        print(middle_slice.min(), middle_slice.max(), flush=True)
        middle_stars = feat.blob_log(middle_slice, max_sigma=10, num_sigma=20, threshold=.01)
        middle_stars = [(y+1523, x+2009, r) for (y, x, r) in middle_stars]
        self.stars = np.concatenate((self.stars, middle_stars), axis=0)
        '''
#        self.stars = np.array([[1773, 2462, 6.7/math.sqrt(2)]])

        # self.stars = feat.blob_doh(self.img, max_sigma=30, threshold=.01)
        #self.stars = np.array([np.array(x) for x in st])
        self.stars[:, 2] = self.stars[:, 2] * math.sqrt(2) 
        
        # maximum radius 40, minimum 2
#        self.stars = [s for s in self.stars if s[2] < 10]
       # print(self.stars)

        # convert to array
#        self.stars = [Star(x, y, self.calc_star_radius_gaussian(x, y, 8, 100)) for (x, y, r) in self.stars]
        self.stars = [Star(x, y, r) for (x, y, r) in self.stars]
#        self.stars = [Star(x, y, 8.) for (x, y, r) in self.stars]
#        self.stars = [Star(x, y, self.calc_star_radius_annulus(x, y)) for (x, y, r) in self.stars]
        print("Done")

    def plot(self):
        if isinstance(self.img[0][0], list):
            plt.imshow(self.img, interpolation='none')
        else:
            plt.imshow(self.img, cmap='gray', interpolation='none')
        plt.gca().invert_yaxis()
        if hasattr(self, 'stars'):
            for star in self.stars:
                c = plt.Circle((star.y, star.x), star.r, color='red', linewidth=2, fill=False)
                plt.gca().add_patch(c)
        plt.show()

    # calculates flux in circle (x, y, r)=circ
    def get_circ_flux(self, circ):
        x_, y_, r_ = circ

        # iterate over square
        flux = 0
        for x in range(max(0, int(x_ - r_)), min(self.width - 1, int(x_ + r_)+1)):
            for y in range(max(0, int(y_ - r_)), min(self.height - 1, int(y_ + r_)+1)):
                r = math.sqrt((x-x_)**2 + (y-y_)**2)
                # check if pixel is within circle
                if r <= r_:
                    flux += self.raw_data[x][y]

        return flux

    def calc_flux(self):
        for star in self.stars:
            star.flux = self.get_circ_flux((star.x, star.y, star.r))
#            print(star.flux)

    def calc_star_radius_annulus(self, x, y):
        RL = []
        FL = []
        for i in range(1, 50):
            r = i / 5.
            RL.append(r)
            flux = self.get_circ_flux((x, y, r))/(math.pi * r**2)
            c = 2
            annulus_flux = self.get_circ_flux((x, y, r+c))/(math.pi * (r+c)**2)
            FL.append(flux / annulus_flux)#math.sqrt(annulus_flux))
        plt.plot(RL, FL)
        plt.show()
    
    def calc_star_radius_gaussian(self, x_, y_, max_radius, num_radius):
        radius_list = np.array(range(0, num_radius)) * 1. / num_radius * max_radius
        radius_list = radius_list[radius_list >= 1]

        # fit on radius
        stddevs = []
        for r_ in radius_list:
            sqrs = []
            # for all pixels within circle
            x_range = (max(0, int(x_ - r_)), min(self.width - 1, int(x_ + r_)+1))
            y_range = (max(0, int(y_ - r_)), min(self.height - 1, int(y_ + r_)+1))
            # TODO circle slice
#            print(self.raw_data, flush=True)
            nice_slice = self.raw_data[x_range[0]:x_range[1], y_range[0]:y_range[1]]#slice(*x_range), slice(*y_range)]) 
#            print(nice_slice)
            #print(x_range, y_range, nice_slice, flush=True)
            height = np.max(nice_slice)
           # print(height)
            for x in range(*x_range):
                for y in range(*y_range):
                    r = math.sqrt((x-x_)**2 + (y-y_)**2)
                    if r <= r_:
                        sqrs.append((self.raw_data[x][y] - gaussian_2D(height, x_, y_, (r_))(x, y))**2)
            stddev = math.sqrt(sum(sqrs) / len(sqrs))
 #           if stddev == 0.0:
 #               print("r_: {}, height: {}, sqrs: {}".format(r_, height, sqrs))
#            print("stddev: {}".format(stddev))
            stddevs.append(stddev)
        
        for i, stddev in enumerate(stddevs):
            if stddev == 0.0:
                continue
            
        print(radius_list[min(enumerate(stddevs), key=operator.itemgetter(1))[0]] * 2)
        return radius_list[min(enumerate(stddevs), key=operator.itemgetter(1))[0]] * 2


class FitsList:
    # dimension: (x1, x2, y1, y2)
    def __init__(self, L_path, R_path, V_path, B_path, dimensions=(), scale_func=np.arcsinh):
        # holds all the Image instances
        self.data = []
        self.scale_func = scale_func


        # create L image, find stars & flux
        L_img = Image(L_path)
        if len(dimensions):
            L_img.slice(*dimensions)

        L_img.scale(scale_func, .5, 99.5)
        print(L_img.img.min(), L_img.img.max())
    
        #L_img.normalize_raw()
        #print(L_img.raw_data.min(), L_img.raw_data.max())
        L_img.find_stars()
        L_img.calc_flux()
        self.data.append(copy.deepcopy(L_img))

        for i, path in enumerate([R_path, V_path, B_path]):
            # create new image, calculate flux using L_img coordinates
            tmp_img = Image(path, bg=[281, 353, 144][i])
            if len(dimensions):
                tmp_img.slice(*dimensions)
            
            tmp_img.scale(scale_func, .5, 99.5)
            tmp_img.stars = L_img.stars
            tmp_img.calc_flux()

            self.data.append(copy.deepcopy(tmp_img))

    def plot(self):
        RGB = tuple([x.img for x in self.data[1:]])
        img = np.dstack(RGB)

        # make a dummy instance of Image, to utilize the plot function
        tmp_img = Image()
        tmp_img.img = img
        tmp_img.stars = self.data[0].stars

        tmp_img.plot()



