import funk

import astropy
from astropy.io import fits
import numpy as np
import pandas as pd
import skimage.morphology as morph
import skimage.exposure as skie
import skimage.feature as feat
import matplotlib.pyplot as plt
from math import *

img = fits.getdata("M3/M3-R.fit")[1000:3500, 1000:3500]
limg = np.arcsinh(img)
limg = limg / limg.max()
low = np.percentile(limg, .5)
high = np.percentile(limg, 99.5)
opt_img = skie.exposure.rescale_intensity(limg, in_range=(low, high))

local_maxima = morph.local_maxima(opt_img)
lm2 = morph.h_maxima(opt_img, h=.5)

lm3 = np.array(local_maxima, dtype=bool)
#print(lm3)
lm3_ = morph.remove_small_objects(lm3, 2)
#print(lm3_)

#print(opt_img)

lm4 = np.copy(local_maxima)
lim = 0.1
for y in range(0, len(local_maxima)):
    for x in range(0, len(local_maxima[0])):
        if local_maxima[x][y]:
            if opt_img[x][y] < lim:
                lm4[x][y] = False


bl = feat.blob_log(opt_img, max_sigma=30, num_sigma=10, threshold=.1)
bl[:, 2] = bl[:, 2] * sqrt(2)
bl = bl[bl[:, 2] < 40]



fig, ax = plt.subplots(2, 3)
ax[0][0].imshow(opt_img, cmap='gray', interpolation='none')
ax[0][1].imshow(local_maxima, cmap='gray', interpolation='none')
ax[1][0].imshow(lm2, cmap='gray', interpolation='none')
ax[1][1].imshow(lm3_, cmap='gray', interpolation='none')
ax[1][2].imshow(lm4, cmap='gray', interpolation='none')
ax[0][2].imshow(opt_img, cmap='gray', interpolation='none')
for blob in bl:
    y, x, r = blob
    c = plt.Circle((x, y), r, color='red', linewidth=2, fill=False)
    ax[0][2].add_patch(c)
plt.show()